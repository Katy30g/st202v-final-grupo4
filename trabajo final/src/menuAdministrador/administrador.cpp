/*
 * administrador.cpp
 *
 *  Created on: 29 may. 2018
 *      Author: PC20
 */
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "../menuCliente/menuCliente.hpp"
#include "../menuEntrada/menuEntrada.hpp"
#include "../menuEmpleado/menuEmpleado.hpp"
#include <fstream>
using namespace std;
void menuAdministrador();
void verComo(){
	int n, m;
	cout<<"Ingresar como: "<<endl;
	cout<<"1. Cliente"<<endl;
	cout<<"2. Empleado "<<endl;
	cout<<"3. Administrador "<<endl;

	cin>>n;

	bool regresar;
	do{

		cout<<"Digite su n�mero "<<endl;
		switch(n){
			regresar= '1';
			case(1):
					menuCliente();
				break;
			case(2):
					menuEmpleado();
				break;
			case(3):
					menuAdministrador();
				break;
		}
		cout<<"�Desea regresar a --verComo--?"<<endl;
		cout<<"1. no"<<endl;
		cout<<"2. si"<<endl;
		cin>>m;
		if(m==1){
			regresar= '0';
		}

	}while (regresar == '1');
}

void reporteDelUsuario(){
	ifstream archivo;
	string texto;
	archivo.open("src\\datos\\registroUsuarios.txt",ios::in);
	if(archivo.fail()){
		cout<<"No se puede abrir el archivo"<<endl;
		exit(1);
	}
	while (!archivo.eof()){
		getline(archivo,texto);
		cout<<texto<<endl;
	}
	archivo.close();
}

void reporteCliente(){
	ifstream archivo;
	string texto;
	archivo.open("src\\datos\\registroCliente.txt",ios::in);
	if(archivo.fail()){
		cout<<"No se puede abrir el archivo"<<endl;
		exit(1);
	}
	while (!archivo.eof()){
		getline(archivo,texto);
		cout<<texto<<endl;
	}
	archivo.close();
}

void reporteEmpleado(){
	ifstream archivo;
	string texto;
	archivo.open("src\\datos\\registroEmpleado.txt",ios::in);
	if(archivo.fail()){
		cout<<"No se puede abrir el archivo"<<endl;
		exit(1);
	}
	while (!archivo.eof()){
		getline(archivo,texto);
		cout<<texto<<endl;
	}
	archivo.close();
}



void menuAdministrador(){
	int p;
	cout<<"Qu� desea hacer"<<endl;
	cin>>p;
	cout<<"1. Ver como"<<endl;
	cout<<"2. Ingresar al reporte de usuario"<<endl;
	cout<<"3. Ingresar al reporte del cliente"<<endl;
	cout<<"4. Ingresar al reporte del empleado"<<endl;
	switch(p){
				case(1):
						verComo();
					break;
				case(2):
						reporteDelUsuario();
					break;
				case(3):
						reporteCliente();
					break;
				case(4):
						reporteEmpleado();
					break;
			}
}

