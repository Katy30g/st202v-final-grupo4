/* menuEmpleado.cpp
 *
 *  Created on: 29 may. 2018
 *      Author: usuario
 */
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <ctime>
using namespace std;

bool verificarProducto(string nombreProd){
	bool j=1;
	ifstream archivo;
	string texto,comparar;
		archivo.open("src//datos//catalogo.txt",ios::in);
		if(archivo.fail()){
			cout<<"No se puede abrir el archivo"<<endl;
			exit(1)
			;
		}
		while (!archivo.eof()){
			getline(archivo,texto);

			int i=0;

			while( texto[i] != ','){
				comparar= comparar+texto[i];
				i++;
			}

			if(comparar == nombreProd){
				j=0;
			}

			comparar="";

		}
		archivo.close();
		return j;
}
void anadirProducto(string nombreProd, string stock, string pCosto, string pVenta, string descripcion){
	ofstream archivo;
	string texto;
	string comparar;
		archivo.open("src//datos//catalogo.txt",ios::app);
		if(archivo.fail()){
			cout<<"No se puede abrir el archivo"<<endl;
			exit(1)
			;
		}
			texto = nombreProd + ',';
			for (int i=texto.length(); i<=30 ; i++){
				texto=texto+' ';
			}

			texto=texto+stock+',';

			for (int i=texto.length(); i<=35;i++ ){
				texto= texto +' ';}

			if (texto==nombreProd){
			}
			texto = texto + pCosto + ',';

			for (int i=texto.length(); i<=42; i++ ){
				texto=texto+' ';
			}

			texto=texto+descripcion;
			archivo<<endl;
			archivo<<texto;

		archivo.close();
}
void eliminarProducto(string nombreProd){
	ifstream archivo;
	vector <string> datos;
	string texto,comparar;
	archivo.open("src//datos//catalogo.txt",ios::in);
	if(archivo.fail()){
		cout<<"No se puede abrir el archivo"<<endl;
		exit(1);
	}else{
		while (!archivo.eof()){
		getline(archivo,texto);
		int i=0;

			comparar="";
			while( texto[i] != ','){
				comparar= comparar+texto[i];
				i++;
			}

			if (nombreProd == comparar){
			}else{
				datos.push_back(texto);
			}

		}
		archivo.close();

		ofstream archivo1;
		archivo1.open("src//datos//catalogo.txt",ios::out);
		if(archivo1.fail()){
			cout<<"No se puede abrir el archivo"<<endl;
			exit(1);
		}else{
			int j=0;
			int k=datos.size();
			while (j < k ){
				if (j==0){
					texto=datos[j];
					archivo1 << texto;
					j++;
				}else{
					texto=datos[j];
					archivo1 <<endl;
					archivo1 << texto;
					j++;
				}
			}
		}
	}
}
void registrarOperacion(string nombreProd, string reg){
	ofstream archivo;
	archivo.open("src//datos//registroCatalogo.txt",ios::app);
	if(archivo.fail()){
		cout<<"No se puede abrir el archivo"<<endl;
		exit(1)	;
	}

	time_t now = time(0);
	tm * time = localtime(&now);

	archivo << "Se "<< reg << " el producto '"<< nombreProd<< "' el d�a \""<< time->tm_mday << "/" << time->tm_mon +1 << "/"<< (time->tm_year)-100 << "\" a las" << time->tm_hour << ":"<<time-> tm_min << ":" << time->tm_sec<< endl;
	archivo.close();
}
void editarCatalogo(){
	int a;
	string stock, pCosto, pVenta,descripcion;
	string nombreProd,fecha,hora;

	cout << "                    EDITAR CATALOGO                    "<<endl;
	cout << "-------------------------------------------------------"<<endl;
	cout << "1) A�adir producto al cat�logo"<<endl;
	cout << "2) Eliminar porducto del cat�logo"<<endl;
	cout << "3) Regresar..."<< endl;
	cin >> a;

	switch (a){

	case 1:

		cout << "                    A�ADIR PRODUCTO                    "<<endl;
		cout << "-------------------------------------------------------"<<endl;
		cout << "Ingrese el nomnbre del producto:"<<endl;
		cin.ignore();
		getline(cin,nombreProd);
		if (verificarProducto(nombreProd)){
			cout << "Ingrese cantidad en el stock:"<<endl;
			cin >> stock;
			cout << "Ingrese el precio de costo:"<<endl;
			cin >> pCosto;
			cout << "Ingrese el precio de venta:"<<endl;
			cin >> pVenta;
			cout << "Ingrese descripcion del producto"<<endl;
			cin.ignore();
			getline(cin,descripcion);

			anadirProducto(nombreProd,stock,pCosto,pVenta,descripcion);
			string reg="agrego";
			registrarOperacion(nombreProd, reg);
			editarCatalogo();
		}else{
			cout << "Existe un producto con el mismo nombre"<<endl;
			editarCatalogo();
		}
		break;

	case 2:

		cout << "                   ELIMINAR PRODUCTO                   "<<endl;
		cout << "-------------------------------------------------------"<<endl;
		cout << "Ingrese el nomnbre del producto:"<<endl;
		cin.ignore();
		getline(cin,nombreProd);
			if (!verificarProducto(nombreProd)){
				eliminarProducto(nombreProd);
				string reg="elimino";
				registrarOperacion(nombreProd, reg);
				cout << "El producto "<< nombreProd << " fue eliminado"<< endl;
				editarCatalogo();
			}else{
				cout << "El producto "<< nombreProd << " no fue encontrado" << endl;
				editarCatalogo();
			}
		break;
	default:

		char p;
		cout << "Opcion no disponible" <<endl;
		cout << ""<<endl;
		cout << "�Desea volver al men� 'EDITAR CATALOGO'?  (Y/N)"<<endl;
		cin >> p;

		if (p=='Y' || p== 'y'){
			editarCatalogo();
		}

		break;

	}



}
void mostrarRegistro( string fechaInicio, string fechaFin){
	ifstream archivo;
	vector <string> datos;
	string texto,aux,ini,fin;

	int l=fechaInicio.length();
	aux = fechaInicio;
	int d =  aux.find('/');
	string inid = aux.substr(0,d);
	aux = aux.substr(d+1,l);
	int m = aux.find('/');
	string inim = aux.substr(0,m);
	l=aux.length();
	aux = aux.substr(m+1,l);
	int y=aux.find(' ');
	string iniy=aux.substr(0,y);
	int intini = atoi(iniy.c_str())*1000+ atoi(inim.c_str())*100+atoi(inid.c_str());

	l=fechaFin.length();
	aux = fechaFin;
	d =  aux.find('/');
	inid = aux.substr(0,d);
	aux = aux.substr(d+1,l);
	m = aux.find('/');
	inim = aux.substr(0,m);
	l=aux.length();
	aux = aux.substr(m+1,l);
	y=aux.find(' ');
	iniy=aux.substr(0,y);
	int intfin = atoi(iniy.c_str())*1000+ atoi(inim.c_str())*100+atoi(inid.c_str());

	archivo.open("src//datos//registroCatalogo.txt",ios::in);
	if(archivo.fail()){
		cout<<"No se puede abrir el archivo"<<endl;
		exit(1);
	}else{
		while (!archivo.eof()){
		getline(archivo,texto);

		int ls = texto.find('"')+1;
		l=texto.length();
		aux = texto.substr(ls,l);
		d =  aux.find('/');
		inid = aux.substr(0,d);
		aux = aux.substr(d+1,l);
		m = aux.find('/');
		inim = aux.substr(0,m);
		l=aux.length();
		aux = aux.substr(m+1,l);
		y=aux.find(' ');
		iniy=aux.substr(0,y);
		int f = atoi(iniy.c_str())*1000+ atoi(inim.c_str())*100+atoi(inid.c_str());
		if (intini<= f && f<=intfin){
			datos.push_back(texto);
		}
		}
	}
	archivo.close();
	l =datos.size();
	int i=0;
	while(i<l){
		cout << datos[i]<<endl;
		i++;
	}
}
void registroCatalogo(){
	string fechaInicio, fechaFin;
	cout <<  "                   MOSTRAR REGISTRO                   "<< endl;
	cout <<  "------------------------------------------------------"<< endl;
	cout << "Ingrese fecha desde la cual se mostrara el registro: (dd/mm/aa)"<< endl;
	cin >> fechaInicio;
	cout << "Ingrese hasta la cual se mostrara el registro: (dd/mm/aa)"<< endl;
	cin >> fechaFin;
	mostrarRegistro(fechaInicio, fechaInicio);

	char p;
	cout << "�Desea volver al men� 'MOSTRAR REGISTRO'?  (Y/N)"<<endl;
	cin >> p;

	if (p=='Y' || p== 'y'){
		registroCatalogo();
	}
}
void mostrarVentas(string fechaInicio, string fechaFin){
	ifstream archivo;
		vector <string> datos;
		string texto,aux,ini,fin;

		int l=fechaInicio.length();
		aux = fechaInicio;
		int d =  aux.find('/');
		string inid = aux.substr(0,d);
		aux = aux.substr(d+1,l);
		int m = aux.find('/');
		string inim = aux.substr(0,m);
		l=aux.length();
		aux = aux.substr(m+1,l);
		int y=aux.find(' ');
		string iniy=aux.substr(0,y);
		int intini = atoi(iniy.c_str())*1000+ atoi(inim.c_str())*100+atoi(inid.c_str());

		l=fechaFin.length();
		aux = fechaFin;
		d =  aux.find('/');
		inid = aux.substr(0,d);
		aux = aux.substr(d+1,l);
		m = aux.find('/');
		inim = aux.substr(0,m);
		l=aux.length();
		aux = aux.substr(m+1,l);
		y=aux.find(' ');
		iniy=aux.substr(0,y);
		int intfin = atoi(iniy.c_str())*1000+ atoi(inim.c_str())*100+atoi(inid.c_str());

		archivo.open("src//datos//historialCompras.txt",ios::in);
		if(archivo.fail()){
			cout<<"No se puede abrir el archivo"<<endl;
			exit(1);
		}else{
			while (!archivo.eof()){
			getline(archivo,texto);

			int ls = texto.find('|')+1;
			l=texto.length();
			aux = texto.substr(ls,l);
			d =  aux.find('/');
			inid = aux.substr(0,d);
			aux = aux.substr(d+1,l);
			m = aux.find('/');
			inim = aux.substr(0,m);
			l=aux.length();
			aux = aux.substr(m+1,l);
			y=aux.find(' ');
			iniy=aux.substr(0,y);
			int f = atoi(iniy.c_str())*1000+ atoi(inim.c_str())*100+atoi(inid.c_str());
			if (intini<= f && f<=intfin){
				datos.push_back(texto);
			}
			}
		}
		archivo.close();
		l =datos.size();
		int i=0;
		while(i<l){
			cout << datos[i]<<endl;
			i++;
		}
}
void ventas(){
	string fechaInicio, fechaFin;

	cout << "Ingrese la fecha inicio:(dd/mm/aa)"<< endl;
	cin >> fechaInicio;
	cout << "Ingrese la fecha fin:(dd/mm/aa)"<< endl;
	cin >> fechaFin;
	mostrarVentas( fechaInicio, fechaFin);
}
void mostrarStock(string fechaInicio, string fechaFin){

}
void stock(){
	string fechaInicio, fechaFin;

	cout << "Ingrese la fecha inicio:(dd/mm/aa)"<< endl;
	cin >> fechaInicio;
	cout << "Ingrese la fecha fin:(dd/mm/aa)"<< endl;
	cin >> fechaFin;
	mostrarStock( fechaInicio, fechaFin);
}
void mostrarClientes(){
	ifstream archivo;
	vector <string> datos;
	string texto,aux,ini,fin,cad1,cad2;
	int abc,b;
	archivo.open("src//datos//registroUsuarios.txt",ios::in);
		if(archivo.fail()){
			cout<<"No se puede abrir el archivo"<<endl;
			exit(1);
		}else{
			while (!archivo.eof()){
			getline(archivo,texto);
			cad1=texto;
			b=cad1.find("|");
			cad2=cad1.substr(0,b);
			abc= cad1.length();
			cad1 = cad1.substr(b+1,abc);
			cout<<"Nombre: "<<cad2<<endl;
			b=cad1.find("|");
			cad2=cad1.substr(0,b);
			abc= cad1.length();
			cad1 = cad1.substr(b+1,abc);
			cout<<"Apellido: "<<cad2<<endl;
			b=cad1.find("|");
			cad2=cad1.substr(0,b);
			abc= cad1.length();
			cad1 = cad1.substr(b+1,abc);
			cout<<"Correo: "<<cad2<<endl;
			b=cad1.find("|");
			cad2=cad1.substr(0,b);
			abc= cad1.length();
			cad1 = cad1.substr(b+1,abc);
			cout<<"Celular: "<<cad2<<endl;
			b=cad1.find("|");
			cad2=cad1.substr(0,b);
			abc= cad1.length();
			cad1 = cad1.substr(b+1,abc);
			cout<<"Nombre de Usuario: "<<cad2<<endl;
			b=cad1.length();
			cad2=cad1.substr(0,b);
			cout<<"Contrase�a: "<<cad2<<endl;
			cout<<endl;
			}
		}
		archivo.close();
}
void reporteClientes(){
	mostrarClientes();
}
void menuEmpleado(){
	int a;
	cout << endl;
	cout << "--------------���Bienvenido Empleado!!!---------------"<<endl;
	cout << endl;
	cout << "�Que desea hacer hoy?" << endl;
	cout << "1.- Editar catalogo"<< endl;
	cout << "2.- Mostrar cambios en el catalogo"<< endl;
	cout << "3.- Mostrar ventas"<< endl;
	cout << "4.- Mostrar reporte de Clientes"<<endl;
	cin >> a;
	switch (a){
	case 1:
		editarCatalogo();
		menuEmpleado();
		break;
	case 2:
		registroCatalogo();
		menuEmpleado();
		break;
	case 3:
		ventas();
		menuEmpleado();
		break;
	case 4:
		reporteClientes();
		system ("pause");
		menuEmpleado();
		break;
	default:
		cout << "Esta opcion no es v�lida"<< endl;
		cout << "�Desea volver al menu?(Y/N)"<< endl;
		int s;
		cin >> s;
		if (s=='y' || s=='Y'){
			menuEmpleado();
		}else{
			exit (1);
		}

	}
}
