/*
 * menuEmpleado.hpp
 *
 *  Created on: 29 may. 2018
 *      Author: usuario
 */

#ifndef MENUEMPLEADO_MENUEMPLEADO_HPP_
#define MENUEMPLEADO_MENUEMPLEADO_HPP_

#include <iostream>
#include <string>

using namespace std;

bool verificarProducto(string nombreProd);
void anadirProducto(string nombreProd, string stock, string pCosto, string pVenta, string descripcion);
void eliminarProducto(string nombreProd);
void editarCatalogo();
void registrarOperacion(string nombreProd, string reg);
void mostrarRegistro( string fechaInicio, string fechaFin);
void registroCatalogo();
void ventas();
void stock();
void reporteClientes();
void menuEmpleado();



#endif /* MENUEMPLEADO_MENUEMPLEADO_HPP_ */
