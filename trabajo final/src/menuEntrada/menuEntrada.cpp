/*
 * menuEntrada.cpp
 *
 *  Created on: 30 may. 2018
 *      Author: Eduard
 */

#include<iostream>
#include<fstream>
#include<vector>
#include "../menuEmpleado/menuEmpleado.hpp"
#include "../menuCliente/menuCliente.hpp"
using namespace std;

string nombres,apellidos,correo,telefono;
string user,password;


void guardarUsuario(string user,string password){
	ofstream archivol;
	archivol.open("src/datos/listaClientes.txt",ios::app);
	if (archivol.is_open()){
	archivol<<user<<","<<password<<endl;
	}
	archivol.close();
}

bool validarCuentaReg(string user){
	bool validez;
	ifstream archivo;
	string texto,comparar;
		archivo.open("src/datos/listaClientes.txt",ios::in);
		if(archivo.fail()){
			cout<<"No se puede abrir el archivo"<<endl;
		}
		while (!archivo.eof()){
			getline(archivo,texto);
			int i=0;
			while( texto[i] != ','){
				comparar= comparar+texto[i];
				i++;
			}
			if(comparar == user){
				validez=0;
			}
			comparar="";
		}
		cout<<comparar;
		archivo.close();
		return validez;
}

void registrarUsuario(string nombres,string apellidos,string correo,string telefono,string user,string password){
	ofstream archivol;
		archivol.open("src/datos/registroUsuarios.txt",ios::app);
		if (archivol.is_open()){
		archivol<<nombres<<"|"<<apellidos<<"|"<<correo<<"|"<<telefono<<"|"<<user<<"|"<<password<<endl;
		}
		archivol.close();
}

void ingreso(){
	cout<<"usuario: "<<endl;
	cin>>user;
	cout<<"contrase�a: "<<endl;
	cin>>password;
}

void validacion(){


	if (validarCuentaReg(user)){
			cout<<"���Su registro ha sido completado exitosamente!!!"<<endl;
			cout<<"---------------------------------------------------------"<<endl;
			guardarUsuario(user,password);
			registrarUsuario(nombres,apellidos,correo,telefono,user,password);
		}
		else{
			cout<<"\n-----���El nombre de usuario que ha registrado, ya existe!!!------"<<endl;
			cout<<"...por favor intente con otro usuario\n"<<endl;
			ingreso();
			validacion();
		}

}

void Registrarse(){
	cout<<"------Registro-----"<<endl;
	cout<<"Llene los campos de su registro:"<<endl;
	cout<<"Datos personales:\n"<<endl;
	cin.ignore();
	cout<<"nombres: "<<endl;
	getline(cin,nombres);
	cout<<"Apellidos:"<<endl;
	getline(cin,apellidos);
	cout<<"correo electronico: "<<endl;
	cin>>correo;
	cout<<"telefono:"<<endl;
	cin>>telefono;
	cout<<"\n\n";
	cout<<"Perfil de usuario:\n"<<endl;
	cout<<"nombre de usuario:"<<endl;
	cin>>user;
	cout<<"contrase�a"<<endl;
	cin>>password;
	validacion();
}

bool validarCuentaIngCliente(string user,string password){
	bool validez=0;
	vector<string>lista;
	ifstream archivoi;
	string texto,comparar;
	string UserPass=user+','+password;
		archivoi.open("src/datos/listaClientes.txt",ios::in);
			if(archivoi.is_open()){
				while (!archivoi.eof()){
					getline(archivoi,texto);
					lista.push_back(texto);
				}
				archivoi.close();
			}else{
				cout<<"No se puede abrir el archivo"<<endl;
			}

	int k=lista.size();
	for (int i=0;i<k;i++){
		if(UserPass==lista.at(i)){
			validez=1;
		}
	}

	return validez;
}


bool validarCuentaIngAdministrador(string user,string password){
	bool validez=0;
	vector<string>lista;
	ifstream archivoi;
	string texto,comparar;
	string UserPass=user+','+password;
		archivoi.open("src/datos/listaAdministradores.txt",ios::in);
			if(archivoi.is_open()){
				while (!archivoi.eof()){
					getline(archivoi,texto);
					lista.push_back(texto);
				}
				archivoi.close();
			}else{
				cout<<"No se puede abrir el archivo"<<endl;
			}

	int k=lista.size();
	for (int i=0;i<k;i++){
		if(UserPass==lista.at(i)){
			validez=1;
		}
	}

	return validez;
}


bool validarCuentaIngEmpleado(string user,string password){

	bool validez=0;
	vector<string>lista;
	ifstream archivoi;
	string texto,comparar;
	string UserPass=user+','+password;
		archivoi.open("src/datos/listaEmpleados.txt",ios::in);
			if(archivoi.is_open()){
				while (!archivoi.eof()){
					getline(archivoi,texto);
					lista.push_back(texto);
				}
				archivoi.close();
			}else{
				cout<<"No se puede abrir el archivo"<<endl;
			}

	int k=lista.size();
	for (int i=0;i<k;i++){
		if(UserPass==lista.at(i)){
			validez=1;
		}
	}

	return validez;
}

void Ingresar(){
	string resp;
	cout<<"------------Bienvenido a Cupcakes JCC----------"<<endl;
	cout<<"Ingrese su cuenta:\n "<<endl;
	ingreso();



	if(validarCuentaIngAdministrador(user,password)){
		cout<<"Usted ha iniciado su cuenta correctamente"<<endl;
		cout<<"-------------------------------------------------------"<<endl;
		cout<<"----------���Bienvenido Administrador!!!----------"<<endl;
	}else if(validarCuentaIngEmpleado(user,password)){
		cout<<"Usted ha iniciado su cuenta correctamente\n"<<endl;
		cout<<"-------------------------------------------------------"<<endl;
		menuEmpleado();
	}else if(validarCuentaIngCliente(user,password)){
		cout<<"Usted ha iniciado su cuenta correctamente\n"<<endl;
		cout<<"-------------------------------------------------------"<<endl;
		cout<<"------------���Bienvenido Cliente!!!------------\n"<<endl;
		menuCliente();
	}else{
		cout<<"...datos introducidos incorrectamente..."<<endl;
		cout<<"Por favor, intente nuevamente...\n"<<endl;
		Ingresar();
	}

}

void menuEntrada(){
	int opcion;
	cout<<"\n\n------Bienvenido a cupcakes JCC----"<<endl;
	cout<<"Elija lo que desea hacer: \n"<<endl;

	cout<<"Si tiene una cuenta Ingrese!!"<<endl;
	cout<<"1.Ingresar"<<endl;

	cout<<"�Aun no tiene una cuanta? Registrese!!!"<<endl;
	cout<<"2.Registrarse"<<endl;

	cin>>opcion;

	switch (opcion){
		case 1:Ingresar();
			break;

		case 2:Registrarse();
				menuEntrada();
			break;
		}
}







