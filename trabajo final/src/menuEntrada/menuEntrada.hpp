/*
 * menuEntrada.hpp
 *
 *  Created on: 30 may. 2018
 *      Author: Eduard
 */

#ifndef MENUENTRADA_MENUENTRADA_HPP_
#define MENUENTRADA_MENUENTRADA_HPP_
#include<string>
using namespace std;

void guardarUsuario(string user,string password);
void Registrarse();
void Ingresar();
bool validarCuentaReg(string user);
void validarCuentaIngCliente(string user,string password);
bool validarCuentaIngAdministrador(string user,string password);
bool validarCuentaIngEmpleado(string user,string password);
void menuEntrada();

#endif /* MENUENTRADA_MENUENTRADA_HPP_ */
